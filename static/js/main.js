jQuery(document).ready(function( $ ) {


  // Testimonials carousel (uses the Owl Carousel library)
  $(".SecondSection-carousel").owlCarousel({
    items : 4,
    autoplay: true,
    dots: true,
      gallery: {
          enabled: true
      },
    loop: true,
    duration: 1000,
    autoplaySpeed: 2000,
    stopOnHover : true,
    responsive: { 0: { items: 1 , dots: false },
        780: { items: 2 , dots: false, },
        1000: { items: 3},
        1150: { items: 4 }
    }
  });

  // Clients carousel (uses the Owl Carousel library)
  $(".clients-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 6 }
    }
  });


});




