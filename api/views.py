from django.shortcuts import render
from rest_framework import viewsets

# Create your views here.
from api.serializers import PostSerializer
from blog.models import Post


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.order_by('publishDate')
    serializer_class = PostSerializer