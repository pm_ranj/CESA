from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.template import RequestContext
from django.urls import reverse
from blog.models import Post
from blog.views import postsRender


def homePage(request):
    newestEvents = Post.objects.all().filter(type="event").order_by('publishDate')[:4]
    newestPosts = Post.objects.all().filter(type="article").order_by('publishDate')[:4]

    context = {
        "newestEvents": newestEvents,
        "newestPosts": newestPosts,
    }

    # return render(request, '../templates/home-page.html', context)
    return HttpResponseRedirect(reverse('blog:home', kwargs={'index': 1}))
