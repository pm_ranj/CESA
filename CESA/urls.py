
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from api.views import PostViewSet

router = routers.DefaultRouter()
router.register(r'posts', PostViewSet)

from CESA import views, settings
from blog import views as blogViews
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.homePage),
    path('accounts/', include('accounts.urls')),
    path('blog/', include('blog.urls')),
    path('journal/', blogViews.magsPage, name='journals'),
    path('rings/', include('rings.urls'), name='rings'),
    path('about-us/', blogViews.aboutUs, name='about'),
    path('api/', include(router.urls))


]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)