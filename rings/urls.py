from django.urls import path
from rings import views
app_name = 'rings'
urlpatterns = [
    path('', views.ringProfiles, name='ring-profiles'),
    path('profiles', views.ringProfiles, name='mainPage')

]