from django.db import models

# Create your models here.
from accounts.models import Account


class Rings(models.Model):
    founder = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="founder")
    coFounder = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="co_founder")
    members = models.ManyToManyField(Account)
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=500)
    logo = models.ImageField(upload_to='media/Rings/ring-logos')

    def __str__(self):
        return str(self.title) + " Ring"


