from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from blog import views
from CESA import  settings

app_name = 'blog'

urlpatterns = [
    path('', views.postsRender , name='blog'),
    path('events', views.postsRender, name="events"),
    path('newpost/', views.newPost, name='submit-post'),
    path('posts/add/', views.addNewPost, name='new-post'),
    path('admin-dashboard/', views.blogAdmin, name='blog-admin'),
    path('categories/<cat_slug>/', views.mainPage, name="posts-by-category-notIndexed"),
    path('page-<int:index>/', views.postsRender, name="home"),
    path('posts/<url_slug>/', views.showPost, name="post-view"),
    path('page-<int:index>/category=<cat_slug>', views.postsByCategory, name='posts-by-category'),
]