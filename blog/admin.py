from django.contrib import admin

# Register your models here.
from blog.models import Post, Comment, Category, Journal


class PostManager(admin.ModelAdmin):
    list_display = ('title', 'author', 'approved', 'submitDate', 'publishDate')


class CommentManager(admin.ModelAdmin):
    list_display = ('post', 'email', 'approved', 'submitDate', 'publishDate')

admin.site.register(Post, PostManager)
admin.site.register(Comment, CommentManager)
admin.site.register(Category)
admin.site.register(Journal)