from django import forms

from blog.models import Post


class NewPostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = "__all__"
        exclude = ['publishDate', 'approved', 'author', 'submitDate']
        widgets= {
            'previewImage': forms.FileInput(attrs={
                'accept': 'image/*',
            }),
        }