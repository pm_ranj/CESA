import math
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from blog.forms import NewPostForm
from accounts.models import Account
from django.contrib.auth import authenticate

from blog.models import Post, Category, Journal


def getAuthStaffUser(request):
    user = None
    if request.user.is_authenticated and not request.user.is_staff:
        username = request.user.username
        user = Account.objects.get(authUser__username=username)
    return user


@login_required
def newPost(request):
    username = request.user.username
    form = NewPostForm()
    user = Account.objects.get(authUser__username=username)
    if user.accessLevel in ['admin', 'author']:
        if request.method == "POST":
            if form.is_valid():
                form.author = Account.objects.get(authUser__username=username)
                form.save()


@login_required
def addNewPost(request):
    username = request.user.username
    user = Account.objects.get(authUser__username=request.user.username)
    if user.accessLevel == "admin":

        if request.method == "POST":
            form = NewPostForm(request.POST, request.FILES)

            if form.is_valid():
                print(request.FILES.dict())
                post = form.save(commit=False)
                post.author = Account.objects.get(authUser__username=username)
                # post.previewImage = request.FILES.dict()
                post.save()
                return HttpResponse("hey there")

            else:
                print(form.errors)
                return HttpResponse("failed")

        else:
            form = NewPostForm()

        return render(request, 'blog/post-editor.html', {"form": form, "user": user})

    return HttpResponse("access denied")


@login_required
def blogAdmin(request):
    username = request.user.username
    user = Account.objects.get(authUser__username=username)
    return render(request, 'blog/home.html', {"user": user})


def magsPage(request):
    user = getAuthStaffUser(request)
    journals = Journal.objects.all().order_by('releaseDate')
    context = {
        'journals_list': journals
    }
    return render(request, 'blog/magazine.html', context)

def aboutUs(request):
    pass
def showPost(request, url_slug):
    post = get_object_or_404(Post, slug=url_slug)
    categories = Category.objects.all()

    context = {
        "post": post,
        "categories": categories,
        "current_page_index": 1,

    }

    return render(request, 'blog/blog-page.html', context)


def postsRender(request, index=1):
    user = None
    if request.user.is_authenticated and not request.user.is_staff:
        username = request.user.username
        user = Account.objects.get(authUser__username=username)

    eachPagePosts = 6
    startPos = (index - 1) * eachPagePosts
    postsNum = Post.objects.count()
    pagesNum = math.ceil(postsNum / eachPagePosts)
    posts = Post.objects.all()
    featured = []

    for p in posts.iterator():
        if p.featured:
            featured.append(p)
    categories = Category.objects.all()
    latestEvent = Post.objects.filter(type='event').first()

    pageData = {
        "page_number": pagesNum,
        "current_page": index,
    }
    context = {
        "user": user,
        "posts_list": posts[startPos: startPos + eachPagePosts],
        "posts_featured": featured,
        "latest_event": latestEvent,
        "categories": categories,
        "pages_number": pagesNum,
        "current_page_index": index,
        "next_page_index": index + 1,
        "previous_page_index": index - 1,
        "current_category_selected": None
    }

    return render(request, 'blog/blog.html', context)


def postsByCategory(request, index=1, cat_slug=None):
    user = getAuthStaffUser(request)
    eachPagePosts = 6
    startPos = (index - 1) * eachPagePosts
    postsNum = Post.objects.count()
    pagesNum = math.ceil(postsNum / eachPagePosts)
    posts = Post.objects.all()
    featured = []

    for p in posts.iterator():
        if p.featured:
            featured.append(p)
    categories = Category.objects.all()

    latestEvent = Post.objects.filter(type='event').first()

    if cat_slug != None:
        posts_list = posts.filter(category__slug=cat_slug)[startPos: startPos + eachPagePosts]
    else:
        posts_list = posts[startPos: startPos + eachPagePosts]


    context = {
        "user": user,
        "posts_list": posts_list,
        "posts_featured": featured,
        "latest_event": latestEvent,
        "categories": categories,
        "pages_number": pagesNum,
        "current_page_index": index,
        "next_page_index": index+1,
        "previous_page_index": index-1,
        "current_category_selected": cat_slug
    }

    return render(request, 'blog/blog.html', context)


def mainPage(request, cat_slug=None):
    postsByCategory(request, index=1, cat_slug=cat_slug)


def downloadJournalFile(request, journal_id):
    pass


def getNextBlogPage(request, current_index, cat_slug=None):
    if cat_slug != None:
        postsRender(request, current_index + 1)
    else:
        postsByCategory(request, current_index + 1, cat_slug)


def getPreviousBlogPage(request, current_index, cat_slug=None):
    if current_index - 1 > 0:
        if cat_slug != None:
            postsRender(request, current_index - 1)
        else:
            postsByCategory(request, current_index - 1, cat_slug)