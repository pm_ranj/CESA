import os
from django.db import models
from django.urls import reverse
from taggit.managers import TaggableManager
from django.utils import timezone

from CESA import settings
from accounts.models import Account as Author


class Category(models.Model):
    title = models.CharField(primary_key=True, max_length=50, unique=True)
    slug = models.SlugField(unique=True, max_length=100)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        pass

    class Meta:
        ordering = ['title']


class Post(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    language = models.CharField(max_length=2, choices=[('fa', 'FA'), ('en', 'EN')], default='fa')
    type = models.CharField(max_length=10,
                            choices=[('article', 'article'), ('event', 'event'), ], default='article', )
    featured = models.BooleanField(default=False)
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=500, unique=True)
    category = models.ManyToManyField(Category, related_name='category', db_index=True, blank=True)
    submitDate = models.DateTimeField(default=timezone.now)
    publishDate = models.DateTimeField(blank=True, null=True)
    previewImage = models.ImageField(blank=True, upload_to='post-images')
    previewText = models.TextField(max_length=300)
    text = models.TextField(null=True)
    tags = TaggableManager()
    approved = models.BooleanField(default=False)


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:post-view', kwargs={'url_slug': self.slug})

    class Meta:
        ordering = ['-publishDate']


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, )
    name = models.CharField(max_length=50, blank=False)
    email = models.EmailField(max_length=200, null=True)
    text = models.TextField()
    reply = models.TextField(blank=True, null=True)
    submitDate = models.DateTimeField(default=timezone.now)
    publishDate = models.DateTimeField(blank=True, null=True)
    approved = models.BooleanField(default=False)

    def __str__(self):
        return self.email


def fileHandler(instance, filename):
    ext = filename.split('.')[1]
    filename = "%s-No-%d.%s" % (instance.releaseDate, instance.number, ext)
    print(filename)
    print(os.path.join(settings.MEDIA_ROOT, filename))
    return os.path.join(settings.MEDIA_ROOT, filename)

class Journal(models.Model):
    releaseDate = models.DateField(auto_now=True)
    description = models.TextField(blank=True)
    number = models.PositiveIntegerField(unique=True)
    type = models.CharField(max_length=50, choices=[('regular', 'regular'), ('special', 'special')], default='regular')
    electronicFile = models.FileField(upload_to=fileHandler)
    coverImage = models.ImageField(upload_to="journals/journal-covers/")

    def __str__(self):
        return "No. #" + str(self.number)


