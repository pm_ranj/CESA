from django.contrib import admin
from accounts.models import Account


class AccountManager(admin.ModelAdmin):
    list_display = ('authUser', 'accessLevel')


admin.site.register(Account, AccountManager)
