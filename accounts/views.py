from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from CESA.views import homePage


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse(homePage))
            else:
                return HttpResponse("user isn't active")
        else:
            return HttpResponse("invalid user")
    else:
        return render(request, 'accounts/login.html', {})


@login_required(login_url='/accounts/login')
def user_logout(request):

    logout(request)
    return render(request, 'accounts/logout.html',{})


