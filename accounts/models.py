from django.db import models
from django.contrib.auth.models import User


class Account(models.Model):
    authUser = models.OneToOneField(User, on_delete=models.CASCADE)
    studentID = models.IntegerField(unique=True)
    profileImage = models.ImageField(upload_to='media/accounts/profile-photo', null=True)
    accessLevel = models.CharField(max_length=50, choices=[
        ('admin', 'admin'),
        ('author', 'author'),
        ('regular', 'regular')
    ])
    identity = models.CharField(max_length=50, choices=[('student', 'student'),('other', 'other')], null=True)

    def __str__(self):
        return str(self.authUser.username)




